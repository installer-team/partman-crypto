build:
	$(MAKE) -C blockdev-wipe
	$(MAKE) -C blockdev-opal-supported

clean:
	$(MAKE) -C blockdev-wipe clean
	$(MAKE) -C blockdev-opal-supported clean
