# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of bg.po to Bulgarian
# Bulgarian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
#
# Translations from iso-codes:
#     Translations taken from ICU SVN on 2007-09-09
# Copyright (C)
#   (translations from drakfw)
#   - further translations from ICU-3.9
# Translation of ISO 639 (language names) to Bulgarian
# Copyright (C) 2010 Free Software Foundation, Inc.
#
# Copyright (C)
#
# Ognyan Kulev <ogi@fmi.uni-sofia.bg>, 2004, 2005, 2006.
# Nikola Antonov <nikola@linux-bg.org>, 2004.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#   Free Software Foundation, Inc., 2004.
#   Georgi Georgiev <assenov_g@operamail.com>, 2001, 2004.
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Ognyan Kulev <ogi@fmi.uni-sofia.bg>, 2004.
#   Damyan Ivanov <dmn@debian.org>, 2006, 2007, 2008, 2009, 2010.
#   Roumen Petrov <transl@roumenpetrov.info>, 2010.
# Damyan Ivanov <dmn@debian.org>, 2006-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: bg\n"
"Report-Msgid-Bugs-To: partman-crypto@packages.debian.org\n"
"POT-Creation-Date: 2024-05-07 20:02+0000\n"
"PO-Revision-Date: 2023-04-06 18:54+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#. Type: text
#. Description
#. File system name
#. Keep translations short enough
#. :sl3:
#: ../partman-crypto.templates:1001
msgid "physical volume for encryption"
msgstr "физически том за шифроване"

#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#. Should be kept very short or unstranslated
#. :sl3:
#: ../partman-crypto.templates:2001
msgid "crypto"
msgstr "crypto"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:3001
msgid "Device-mapper (dm-crypt)"
msgstr "Device-mapper (dm-crypt)"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:4001
msgid ""
"Firmware-backed self-encrypting disk (vendor-implemented OPAL with LUKS2)"
msgstr ""

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. :sl3:
#: ../partman-crypto.templates:6001
msgid "not active"
msgstr "неактивен"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:7001
msgid "Encryption method:"
msgstr "Метод на шифроване:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid "Encryption method for this partition:"
msgstr "Шифроване на този дял:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid ""
"Changing the encryption method will set other encryption-related fields to "
"their default values for the new encryption method."
msgstr "Промяната на метода на шифроване ще промени останалите настройки на шифроването според стойностите по подразбиране на новия метод."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:9001
msgid "Encryption:"
msgstr "Шифроване:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:10001
msgid "Encryption for this partition:"
msgstr "Шифроване за този дял:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:11001
msgid "Key size:"
msgstr "Големина на ключ:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:12001
msgid "Key size for this partition:"
msgstr "Големина на ключ за този дял:"

#. Type: text
#. Description
#. An initialization vector is the initial value used to seed
#. the encryption algorithm
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:13001
msgid "IV algorithm:"
msgstr "Алгоритъм за ИВ:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid "Initialization vector generation algorithm for this partition:"
msgstr "Алгоритъм за създаване на инициализационен вектор за този дял:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid ""
"Different algorithms exist to derive the initialization vector for each "
"sector. This choice influences the encryption security. Normally, there is "
"no reason to change this from the recommended default, except for "
"compatibility with older systems."
msgstr ""
"Съществуват различни алгоритми за получаване на инициализационния вектор "
"(ИВ) за всеки сектор. Сигурността на шифроването зависи от избора на "
"алгоритъма за ИВ. Обикновено алгоритъмът, който се предлага по подразбиране "
"е подходящ, освен ако се търси съвместимост със стари системи."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:15001
msgid "Encryption key:"
msgstr "Ключ за шифроване:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:16001
msgid "Type of encryption key for this partition:"
msgstr "Вид на шифроващия ключ за този дял:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:17001
msgid "Encryption key hash:"
msgstr "Хеш на ключа:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid "Type of encryption key hash for this partition:"
msgstr "Вид на хеша на шифроващия ключ за този дял:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid ""
"The encryption key is derived from the passphrase by applying a one-way hash "
"function to it. Normally, there is no reason to change this from the "
"recommended default and doing so in the wrong way can reduce the encryption "
"strength."
msgstr ""
"Шифроващият ключ се получава чрез прилагане на еднопосочна хешираща функция "
"към въведената парола. Промяната на предложения по подразбиране метод може "
"да доведе до намаляване на ефективността на шифроването."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:19001 ../partman-crypto.templates:20001
msgid "OPAL self-encryption nested with dm-crypt:"
msgstr ""

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:20001
msgid ""
"Some disks support self-encryption. This is a hardware feature that, if "
"enabled via this option, will be used together with dm-crypt to provide a "
"double layer of encryption."
msgstr ""

#. Type: text
#. Description
#. This shows up in a screen summarizing options and will be followed
#. by "yes" or "no"
#. :sl3:
#: ../partman-crypto.templates:21001
msgid "Erase data:"
msgstr "Изтриване на данните:"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:22001
msgid "no"
msgstr "не"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:23001
msgid "yes"
msgstr "да"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:24001
msgid "Erase data on this partition"
msgstr "Изтриване на данните на този дял"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:25001
msgid "Factory reset this entire disk (ALL data will be lost)"
msgstr ""

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001 ../partman-crypto.templates:30001
msgid "Really erase the data on ${DEVICE}?"
msgstr "Наистина ли да бъдат изтрити данните на ${DEVICE}?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001
msgid ""
"The data on ${DEVICE} will be overwritten with zeroes. It can no longer be "
"recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"Данните на ${DEVICE} ще бъдат презаписани с нули. След тази операция "
"първоначалните данни не могат да бъдат възстановени. Това е последната "
"възможност да откажете изтриването."

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:27001 ../partman-crypto.templates:31001
msgid "Erasing data on ${DEVICE}"
msgstr "Изтриване на ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:28001
msgid ""
"The installer is now overwriting ${DEVICE} with zeroes to delete its "
"previous contents. This step may be skipped by cancelling this action."
msgstr ""
"Инсталаторът презаписва ${DEVICE} с нули за да заличи предишното съдържание. "
"Тази стъпка може да бъде прескочена с отказване на операцията."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001 ../partman-crypto.templates:33001
msgid "Erasing data on ${DEVICE} failed"
msgstr "Грешка при изтриването на данните на ${DEVICE}"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001
msgid ""
"An error occurred while trying to overwrite the data on ${DEVICE} with "
"zeroes. The data has not been erased."
msgstr ""
"Появи се проблем при опита за презапис на данните на ${DEVICE} с нули. "
"Данните не са изтрити."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:30001
msgid ""
"The data on ${DEVICE} will be overwritten with random data. It can no longer "
"be recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"Данните на ${DEVICE} ще бъдат презаписани със случайни числа. След тази "
"операция първоначалните данни не могат да бъдат възстановени. Това е "
"последната възможност да откажете изтриването."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:32001
msgid ""
"The installer is now overwriting ${DEVICE} with random data to prevent meta-"
"information leaks from the encrypted volume. This step may be skipped by "
"cancelling this action, albeit at the expense of a slight reduction of the "
"quality of the encryption."
msgstr ""
"Инсталаторът презаписва ${DEVICE} със случайни числа за да предотврати "
"изтичането на мета информация от шифрования том. Тази стъпка може да бъде "
"прескочена с цената на слабо намаляване на качеството на шифроването."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:33001
msgid ""
"An error occurred while trying to overwrite ${DEVICE} with random data. "
"Recovery of the device's previous contents is possible and meta-information "
"of its new contents may be leaked."
msgstr ""
"Грешка при презаписване на данните на ${DEVICE} със случайни числа. Няма "
"гаранция срещу възстановяването на предишното съдържание или изтичането на "
"мета информация за новото съдържание."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:34001
msgid "Setting up encryption..."
msgstr "Настройване на шифроване..."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:35001
msgid "Configure encrypted volumes"
msgstr "Настройване на шифровани томове"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions to encrypt"
msgstr "Няма дялове за шифроване"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions have been selected for encryption."
msgstr "Не са избрани дялове за шифроване."

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid "Required programs missing"
msgstr "Липсват изисквани програми"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid ""
"This build of debian-installer does not include one or more programs that "
"are required for partman-crypto to function correctly."
msgstr ""
"Това издание на инсталатора на Дебиан не включва всички програми, които са "
"необходими за правилното функциониране на partman-crypto."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid "Required encryption options missing"
msgstr "Липсват опции за шифроване"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid ""
"The encryption options for ${DEVICE} are incomplete. Please return to the "
"partition menu and select all required options."
msgstr ""
"Настройките за шифроване на ${DEVICE} са непълни. Върнете се към менюто за "
"манипулиране на дялове и изберете всички необходими опции."

#. Type: text
#. Description
#. :sl3:
#. Translators: this string is used to assemble a string of the format
#. "$specify_option: $missing". If this proves to be a problem in your
#. language, please contact the maintainer and we can do it differently.
#: ../partman-crypto.templates:39001
msgid "missing"
msgstr "липсва"

#. Type: text
#. Description
#. :sl3:
#. What is "in use" is a partition
#: ../partman-crypto.templates:40001
msgid "In use as physical volume for encrypted volume ${DEV}"
msgstr "Използва се като физически том за шифрования том ${DEV}"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid "Encryption package installation failure"
msgstr "Грешка при инсталирането на пакет за шифроване"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"The kernel module package ${PACKAGE} could not be found or an error occurred "
"during its installation."
msgstr ""
"Пакетът с модул(и) на ядро ${PACKAGE} не може да бъде намерен или се е "
"получила грешка по време на инсталацията."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"It is likely that there will be problems setting up encrypted partitions "
"when the system is rebooted. You may be able to correct this by installing "
"the required package(s) later on."
msgstr ""
"Вероятно ще има проблеми по време на начално зареждане, когато системата се "
"опита да настрои шифрованите дялове. Възможно е да ги поправите чрез "
"инсталиране на необходимите пакети по-късно."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid "Write the changes to disk and configure encrypted volumes?"
msgstr "Записване на промените на диска и настройване на шифровани томове?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid ""
"Before encrypted volumes can be configured, the current partitioning scheme "
"has to be written to disk.  These changes cannot be undone."
msgstr ""
"Преди шифрованите томове да може да бъдат настроени, текущата схема на "
"разделянето трябва да бъде записана на диска. Тези промени не могат да бъдат "
"отменени по-късно."

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001 ../partman-crypto.templates:43001
msgid ""
"After the encrypted volumes have been configured, no additional changes to "
"the partitions on the disks containing encrypted volumes are allowed. Please "
"decide if you are satisfied with the current partitioning scheme for these "
"disks before continuing."
msgstr ""
"След като шифрованите томове са вече настроени, не са позволени допълнителни "
"промени на дяловете в дисковете, които ги съдържат. Потвърдете дали текущата "
"схема на разделяне отговаря на очакванията."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:43001
msgid "Keep current partition layout and configure encrypted volumes?"
msgstr "Записване на разделянето на дялове и настройване на шифровани томове?"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "Configuration of encrypted volumes failed"
msgstr "Проблем при настройване на шифровани томове"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "An error occurred while configuring encrypted volumes."
msgstr "Грешка при настройване на шифровани томове."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "The configuration has been aborted."
msgstr "Настройката е прекъсната."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "Initialisation of encrypted volume failed"
msgstr "Грешка при създаването на шифрован том"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "An error occurred while setting up encrypted volumes."
msgstr "Получена е грешка по време на настройване на шифровани томове."

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:46001
msgid "Passphrase"
msgstr "Парола"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:47001
msgid "Keyfile (GnuPG)"
msgstr "Файл с ключ (GnuPG)"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:48001
msgid "Random key"
msgstr "Случаен ключ"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "Unsafe swap space detected"
msgstr "Открито е незащитено място за виртуална памет"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "An unsafe swap space has been detected."
msgstr "Открито е незащитено място за виртуална памет."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"This is a fatal error since sensitive data could be written out to disk "
"unencrypted. This would allow someone with access to the disk to recover "
"parts of the encryption key or passphrase."
msgstr ""
"Това е фатална грешка, тъй като позволява на диска да бъдат записвани "
"нешифровани данни. Това би позволило на хора с достъп до диска да се доберат "
"до части от шифроващи ключове или пароли."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"Please disable the swap space (e.g. by running swapoff) or configure an "
"encrypted swap space and then run setup of encrypted volumes again. This "
"program will now abort."
msgstr ""
"Моля, деактивирайте мястото за виртуална памет (например чрез swapoff) или "
"настройте шифровано място за виртуална памет и тогава стартирайте отново "
"настройката на шифровани дялове."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "Encryption passphrase:"
msgstr "Парола за шифроване:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "You need to choose a passphrase to encrypt ${DEVICE}."
msgstr "Необходима е парола за шифроване на ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid ""
"The overall strength of the encryption depends strongly on this passphrase, "
"so you should take care to choose a passphrase that is not easy to guess. It "
"should not be a word or sentence found in dictionaries, or a phrase that "
"could be easily associated with you."
msgstr ""
"Защитата на шифроването силно зависи от тази парола, така че трябва да "
"изберете парола, която не е лесно да се отгатне. Не трябва да е дума или "
"изречение, което може да се открие в речници, или фраза, която лесно може да "
"бъде асоциирана с компютъра или неговите потребители."

#. Type: password
#. Description
#. :sl3:
#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001 ../partman-crypto.templates:55001
msgid ""
"A good passphrase will contain a mixture of letters, numbers and "
"punctuation. Passphrases are recommended to have a length of 20 or more "
"characters."
msgstr ""
"Добрата парола съдържа смесица от букви, цифри и пунктуация. Паролите за "
"ключове е препоръчително да имат дължина 20 или повече знака."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid "Re-enter passphrase to verify:"
msgstr "Въведете наново паролата за проверка:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid ""
"Please enter the same passphrase again to verify that you have typed it "
"correctly."
msgstr ""
"Въведете отново същата парола, за да бъде проверено дали е написана правилно."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "Passphrase input error"
msgstr "Грешка при въвеждане на парола"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "The two passphrases you entered were not the same. Please try again."
msgstr "Двете пароли, които въведохте, не съвпадат. Моля, опитайте отново."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid "Empty passphrase"
msgstr "Празна парола"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid ""
"You entered an empty passphrase, which is not allowed. Please choose a non-"
"empty passphrase."
msgstr "Въведохте празна парола, което не е позволено. Моля, въведете парола."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:54001
msgid "Use weak passphrase?"
msgstr "Използване на слаба парола?"

#. Type: boolean
#. Description
#. :sl3:
#. Translators: we unfortunately cannot use plural forms here
#. So, you are suggested to use the plural form adapted for
#. MINIMUM=8, which is the current hardcoded value
#: ../partman-crypto.templates:54001
msgid ""
"You entered a passphrase that consists of less than ${MINIMUM} characters, "
"which is considered too weak. You should choose a stronger passphrase."
msgstr ""
"Въведохте парола, която се състои от по-малко от ${MINIMUM} знака, което я "
"прави твърде слаба. Препоръчва се използването по по-сложна парола."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid "OPAL admin password:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"You need to choose a new, or provide the existing, OPAL admin password for "
"${DEVICE}."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"This password will be used to manage the whole device - add new OPAL "
"partitions, remove them, erase them. If this password is lost, the device "
"will need to be factory reset to be recovered, losing all data from all "
"partitions."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
#, fuzzy
msgid "Re-enter OPAL admin password to verify:"
msgstr "Въведете отново паролата,  за да бъде проверена:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
#, fuzzy
msgid ""
"Please enter the same password again to verify that you have typed it "
"correctly."
msgstr ""
"Въведете същата парола за root още веднъж, за да бъде проверено дали сте я "
"въвели правилно."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "Password input error"
msgstr "Грешка при въвеждане на паролата"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Двете пароли, които въведохте, не са едни и същи. Опитайте отново."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid "Empty password"
msgstr "Празна парола"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Въведохте празна парола, което не е позволено. Изберете непразна парола."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid "OPAL PSID:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid ""
"You need to provide the OPAL PSID for ${DEVICE}. This is typically found on "
"the drive's label."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
#, fuzzy
msgid "Re-enter OPAL PSID to verify:"
msgstr "Въведете отново паролата,  за да бъде проверена:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
#, fuzzy
msgid ""
"Please enter the same PSID again to verify that you have typed it correctly."
msgstr ""
"Въведете отново същата парола, за да бъде проверено дали е написана правилно."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
#, fuzzy
msgid "OPAL PSID input error"
msgstr "Грешка при въвеждане на паролата"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
#, fuzzy
msgid "The two OPAL PSIDs you entered were not the same. Please try again."
msgstr "Двете пароли, които въведохте, не са едни и същи. Опитайте отново."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
msgid "Empty OPAL PSID"
msgstr ""

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
#, fuzzy
msgid ""
"You entered an empty PSID, which is not allowed. Please provide a non-empty "
"PSID."
msgstr ""
"Въведохте празна парола, което не е позволено. Изберете непразна парола."

#. Type: entropy
#. Description
#. :sl3:
#: ../partman-crypto.templates:63001
msgid "The encryption key for ${DEVICE} is now being created."
msgstr "Създаване на ключа за шифроване на ${DEVICE}."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:64001
msgid "Key data has been created successfully."
msgstr "Ключът беше създаден успешно."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "Keyfile creation failure"
msgstr "Грешка при създаване на файл за ключ"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "An error occurred while creating the keyfile."
msgstr "Получена е грешка при създаване на файла за ключ."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
#, fuzzy
msgid "Configure encryption without separate /boot?"
msgstr ""
"не се поддържа главна файлова система btrfs за без отделен дял за /boot"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
#, fuzzy
msgid ""
"You have selected the root file system to be stored on an encrypted "
"partition. When using GRUB, usually this feature requires a separate /boot "
"partition on which the kernel and initrd can be stored. This is not required "
"when using systemd-boot/UKIs, as the ESP is used instead."
msgstr ""
"Избрахте кореновата файлова система да бъде върху шифрован дял. Това изисква "
"използването на отделен дял за /boot, който да съдържа ядрото и друга "
"информация, необходима за началното зареждане на операционната система."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid "Encryption configuration failure"
msgstr "Грешка при настройване на шифроване"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You have selected the /boot file system to be stored on an encrypted "
"partition. This is not possible because the boot loader would be unable to "
"load the kernel and initrd. Continuing now would result in an installation "
"that cannot be used."
msgstr ""
"Избрахте файловата система за зареждане на операционната система (/boot) да "
"бъде върху шифрован дял. Тази възможност не е достъпна, понеже програмата за "
"първоначално зареждане няма да може да зареди ядрото от шифрован дял. "
"Продължаването въпреки това предупреждение ще доведе до неизползваема "
"инсталация."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You should go back and choose a non-encrypted partition for the /boot file "
"system."
msgstr ""
"Моля, върнете се обратно и изберете нешифрован дял за файловата система /"
"boot."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid "Are you sure you want to use a random key?"
msgstr "Сигурен(-на) ли сте, че искате да използвате случаен ключ?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"You have chosen a random key type for ${DEVICE} but requested the "
"partitioner to create a file system on it."
msgstr ""
"Избрано е използването на случаен ключ за ${DEVICE}, но е указано и "
"създаване на файлова система."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"Using a random key type means that the partition data is going to be "
"destroyed upon each reboot. This should only be used for swap partitions."
msgstr ""
"Използването на случаен ключ означава, че данните в дяла ще бъдат "
"унищожавани при всяко рестартиране. Тази възможност трябва да се използва "
"само за дяловете, предназначени за виртуална памет."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "Failed to download crypto components"
msgstr "Грешка при зареждане на компонентите за шифроване"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "An error occurred trying to download additional crypto components."
msgstr "Грешка при опит за изтегляне на допълнителни компоненти за шифроване."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid "Proceed to install crypto components despite insufficient memory?"
msgstr ""
"Да бъде ли направен опит да се инсталират компонентите за шифроване, въпреки "
"че не достига памет?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid ""
"There does not seem to be sufficient memory available to install additional "
"crypto components. If you choose to go ahead and continue anyway, the "
"installation process could fail."
msgstr ""
"Изглежда наличната оперативна памет е недостатъчна за зареждане на "
"допълнителните компоненти, осигуряващи функциите за шифроване. Ако "
"продължите въпреки това, процесът на инсталиране може да не завърши успешно."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Create encrypted volumes"
msgstr "Създаване на шифровани томове"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Finish"
msgstr "Завършване"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "Encryption configuration actions"
msgstr "Настройка на шифроването"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "This menu allows you to configure encrypted volumes."
msgstr "Това меню позволява да се настроят шифрованите томове."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Devices to encrypt:"
msgstr "Устройства за шифроване:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Please select the devices to be encrypted."
msgstr "Изберете устройствата, които да бъдат шифровани."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "You can select one or more devices."
msgstr "Може да изберете едно или повече устройства."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices selected"
msgstr "Не са избрани устройства"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices were selected for encryption."
msgstr "Не са избрани устройства за шифроване."
