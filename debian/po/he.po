# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_he.po to Hebrew
# Hebrew messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Translations from iso-codes:
# Translations taken from ICU SVN on 2007-09-09
# Translations taken from KDE:
# Amit Dovev <debian.translator@gmail.com>, 2007.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Omer Zak <w1@zak.co.il>, 2008, 2010, 2012, 2013.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Free Software Foundation, Inc., 2002,2004.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
# Meni Livne <livne@kde.org>, 2000.
# Free Software Foundation, Inc., 2002,2003.
# Meni Livne <livne@kde.org>, 2000.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Lior Kaplan <kaplan@debian.org>, 2004-2007, 2008, 2010, 2011, 2015, 2017.
# Yaron Shahrabani <sh.yaron@gmail.com>, 2018, 2019, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-crypto@packages.debian.org\n"
"POT-Creation-Date: 2024-05-07 20:02+0000\n"
"PO-Revision-Date: 2024-11-04 08:22+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew (https://www.transifex.com/yaron/teams/79473/he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. File system name
#. Keep translations short enough
#. :sl3:
#: ../partman-crypto.templates:1001
msgid "physical volume for encryption"
msgstr "כרך פיזי להצפנה"

#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#. Should be kept very short or unstranslated
#. :sl3:
#: ../partman-crypto.templates:2001
msgid "crypto"
msgstr "crypto"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:3001
msgid "Device-mapper (dm-crypt)"
msgstr "ממפה התקנים (dm-crypt)"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:4001
msgid ""
"Firmware-backed self-encrypting disk (vendor-implemented OPAL with LUKS2)"
msgstr "כונן שמצפין את עצמו בגיבוי קושחה (OPAL בהטמעת היצרן עם LUKS2)"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. :sl3:
#: ../partman-crypto.templates:6001
msgid "not active"
msgstr "לא פעיל"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:7001
msgid "Encryption method:"
msgstr "שיטת הצפנה:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid "Encryption method for this partition:"
msgstr "שיטת ההצפנה עבור המחיצה:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid ""
"Changing the encryption method will set other encryption-related fields to "
"their default values for the new encryption method."
msgstr ""
"שינוי שיטת ההצפנה ישנה את ערכיהם של שדות אחרים הקשורים להצפנה לערכי בררת "
"המחדל שלהם, בהתאם לשיטת ההצפנה החדשה."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:9001
msgid "Encryption:"
msgstr "הצפנה:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:10001
msgid "Encryption for this partition:"
msgstr "הצפנה עבור מחיצה זו:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:11001
msgid "Key size:"
msgstr "גודל מפתח:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:12001
msgid "Key size for this partition:"
msgstr "גודל המפתח עבור מחיצה זו:"

#. Type: text
#. Description
#. An initialization vector is the initial value used to seed
#. the encryption algorithm
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:13001
msgid "IV algorithm:"
msgstr "אלגוריתם לווקטור אתחול:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid "Initialization vector generation algorithm for this partition:"
msgstr "אלגוריתם יצירת וקטור אתחול של המחיצה:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid ""
"Different algorithms exist to derive the initialization vector for each "
"sector. This choice influences the encryption security. Normally, there is "
"no reason to change this from the recommended default, except for "
"compatibility with older systems."
msgstr ""
"קיימים אלגוריתמים שונים להפעלת וקטור האתחול עבור כל מקטע ומקטע. בחירה זאת "
"משפיעה על אבטחת ההצפנה. בדרך כלל, אין סיבה לשנות זאת מבררת המחדל המומלצת, "
"למעט תאימות למערכות ישנות יותר."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:15001
msgid "Encryption key:"
msgstr "מפתח הצפנה:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:16001
msgid "Type of encryption key for this partition:"
msgstr "סוג מפתח ההצפנה עבור מחיצה זו:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:17001
msgid "Encryption key hash:"
msgstr "גיבוב מפתח הצפנה:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid "Type of encryption key hash for this partition:"
msgstr "סוג גיבוב מפתח (hash) עבור מחיצה זו:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid ""
"The encryption key is derived from the passphrase by applying a one-way hash "
"function to it. Normally, there is no reason to change this from the "
"recommended default and doing so in the wrong way can reduce the encryption "
"strength."
msgstr ""
"מפתח הצפנה זה נגזר על ידי החלת פונקציית גיבוב חד כיוונית על מילת צופן. בדרך "
"כלל, אין סיבה לשנות הגדרה זו למשהו שאינו בררת המחדל ושינוי להגדרה שגויה יכול "
"לפגוע ביעילות ההצפנה."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:19001 ../partman-crypto.templates:20001
msgid "OPAL self-encryption nested with dm-crypt:"
msgstr "OPAL שמצפין את עצמו בקינון עם dm-crypt:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:20001
msgid ""
"Some disks support self-encryption. This is a hardware feature that, if "
"enabled via this option, will be used together with dm-crypt to provide a "
"double layer of encryption."
msgstr ""
"חלק מהכוננים תומכים בהצפנה עצמית. זאת יכולת חומרה, שבהינתן שהאפשרות הזאת "
"נבחרה, תשתלב עם dm-crypt כדי לספק שכבת הצפנה כפולה."

#. Type: text
#. Description
#. This shows up in a screen summarizing options and will be followed
#. by "yes" or "no"
#. :sl3:
#: ../partman-crypto.templates:21001
msgid "Erase data:"
msgstr "למחוק מידע:"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:22001
msgid "no"
msgstr "לא"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:23001
msgid "yes"
msgstr "כן"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:24001
msgid "Erase data on this partition"
msgstr "מחיקת המידע על מחיצה זו"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:25001
msgid "Factory reset this entire disk (ALL data will be lost)"
msgstr "איפוס יצרן של הכונן כולו (כל הנתונים ילכו לאיבוד)"

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001 ../partman-crypto.templates:30001
msgid "Really erase the data on ${DEVICE}?"
msgstr "למחוק את כל המידע על ${DEVICE}?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001
msgid ""
"The data on ${DEVICE} will be overwritten with zeroes. It can no longer be "
"recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"המידע על ${DEVICE} ישוכתב באפסים. הוא לא יהיה ניתן לשחזור לאחר סיום שלב זה. "
"זאת ההזדמנות האחרונה לביטול המחיקה."

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:27001 ../partman-crypto.templates:31001
msgid "Erasing data on ${DEVICE}"
msgstr "המידע על הכונן ${DEVICE} נמחק"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:28001
msgid ""
"The installer is now overwriting ${DEVICE} with zeroes to delete its "
"previous contents. This step may be skipped by cancelling this action."
msgstr ""
"תכנית ההתקנה משכתבת כעת את ${DEVICE} באפסים כדי למחוק את התוכן הקודם שעליו. "
"ניתן לדלג על צעד זה על ידי ביטול הפעולה הזו."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001 ../partman-crypto.templates:33001
msgid "Erasing data on ${DEVICE} failed"
msgstr "מחיקת המידע על ${DEVICE} נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001
msgid ""
"An error occurred while trying to overwrite the data on ${DEVICE} with "
"zeroes. The data has not been erased."
msgstr ""
"התרחשה שגיאה בעת הניסיון לשכתב מידע על ${DEVICE} באפסים. המידע לא נמחק."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:30001
msgid ""
"The data on ${DEVICE} will be overwritten with random data. It can no longer "
"be recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"המידע על ${DEVICE} ישוכתב במידע אקראי. הוא לא יהיה ניתן לשחזור לאחר סיום שלב "
"זה. זאת ההזדמנות האחרונה לביטול המחיקה."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:32001
msgid ""
"The installer is now overwriting ${DEVICE} with random data to prevent meta-"
"information leaks from the encrypted volume. This step may be skipped by "
"cancelling this action, albeit at the expense of a slight reduction of the "
"quality of the encryption."
msgstr ""
"תכנית ההתקנה משכתבת כעת את ${DEVICE} במידע אקראי כדי למנוע דליפות נתוני על "
"מהכרך המוצפן. ניתן לדלג על צעד זה על ידי ביטול הפעולה הזו, במחיר של הפחתה "
"קלה באיכות ההצפנה."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:33001
msgid ""
"An error occurred while trying to overwrite ${DEVICE} with random data. "
"Recovery of the device's previous contents is possible and meta-information "
"of its new contents may be leaked."
msgstr ""
"אירעה שגיאה בעת ניסיון לשכתוב ${DEVICE} במידע אקראי. שחזור התכנים הקודמים של "
"ההתקן אפשריים ונתוני העל של התכנים החדשים שלו עשויים לדלוף."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:34001
msgid "Setting up encryption..."
msgstr "ההצפנה מוגדרת…"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:35001
msgid "Configure encrypted volumes"
msgstr "הגדרת כרכים מוצפנים"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions to encrypt"
msgstr "אין מחיצות להצפנה"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions have been selected for encryption."
msgstr "לא נבחרו מחיצות להצפנה."

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid "Required programs missing"
msgstr "תכניות נחוצות חסרות"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid ""
"This build of debian-installer does not include one or more programs that "
"are required for partman-crypto to function correctly."
msgstr ""
"הרכבה זו של תכנית ההתקנה של דביאן אינה כוללת תכנית אחת או יותר שנחוצות לטובת "
"הפעילות התקינה של partman-crypto."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid "Required encryption options missing"
msgstr "חסרות אפשרות הצפנה נדרשות"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid ""
"The encryption options for ${DEVICE} are incomplete. Please return to the "
"partition menu and select all required options."
msgstr ""
"אפשרויות ההצפנה עבור ${DEVICE} אינן שלמות. יש לחזור אחורה אל תפריט החלוקה "
"למחיצות ולבחור את כל האפשרויות הנדרשות."

#. Type: text
#. Description
#. :sl3:
#. Translators: this string is used to assemble a string of the format
#. "$specify_option: $missing". If this proves to be a problem in your
#. language, please contact the maintainer and we can do it differently.
#: ../partman-crypto.templates:39001
msgid "missing"
msgstr "חסר"

#. Type: text
#. Description
#. :sl3:
#. What is "in use" is a partition
#: ../partman-crypto.templates:40001
msgid "In use as physical volume for encrypted volume ${DEV}"
msgstr "בשימוש ככרך פיזי עבור כרך מוצפן ${DEV}"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid "Encryption package installation failure"
msgstr "התקנת חבילת ההצפנה נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"The kernel module package ${PACKAGE} could not be found or an error occurred "
"during its installation."
msgstr ""
"חבילת מודול הליבה ${PACKAGE} לא נמצאה, או שהתרחשה שגיאה בזמן ההתקנה שלה."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"It is likely that there will be problems setting up encrypted partitions "
"when the system is rebooted. You may be able to correct this by installing "
"the required package(s) later on."
msgstr ""
"כנראה תופענה תקלות בהגדרת מחיצות מוצפנות בעת הפעלת המערכת מחדש. יכול להיות "
"שניתן לתקן זאת על ידי התקנת החבילות הנחוצות בהמשך."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid "Write the changes to disk and configure encrypted volumes?"
msgstr "לכתוב את השינויים לכוננים ולהגדיר כרכים מוצפנים?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid ""
"Before encrypted volumes can be configured, the current partitioning scheme "
"has to be written to disk.  These changes cannot be undone."
msgstr ""
"בטרם יתאפשר להגדיר כרכים מוצפנים, יש לכתוב את הרכב החלוקה הנוכחי לכונן. לא "
"ניתן לבטל שינויים אלה."

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001 ../partman-crypto.templates:43001
msgid ""
"After the encrypted volumes have been configured, no additional changes to "
"the partitions on the disks containing encrypted volumes are allowed. Please "
"decide if you are satisfied with the current partitioning scheme for these "
"disks before continuing."
msgstr ""
"לאחר הגדרת כרכים מוצפנים, לא ניתן לערוך שינויים נוספים במחיצות בכונן שמכיל "
"את הכרכים המוצפנים. מוטב לקבל החלטה על שביעות רצונך בנוגע להרכב החלוקה "
"הנוכחי של הכוננים האלה בטרם המשך התהליך."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:43001
msgid "Keep current partition layout and configure encrypted volumes?"
msgstr "לשמור את הפריסה הקיימת של המחיצות ולהגדיר כרכים מוצפנים?"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "Configuration of encrypted volumes failed"
msgstr "הגדרת כרכים מוצפנים נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "An error occurred while configuring encrypted volumes."
msgstr "אירעה שגיאה בזמן הגדרת כרכים מוצפנים."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "The configuration has been aborted."
msgstr "תהליך ההגדרה בוטל."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "Initialisation of encrypted volume failed"
msgstr "אתחול כרך מוצפן נכשל"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "An error occurred while setting up encrypted volumes."
msgstr "אירעה שגיאה בעת הגדרת כרכים מוצפנים."

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:46001
msgid "Passphrase"
msgstr "מילת צופן"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:47001
msgid "Keyfile (GnuPG)"
msgstr "קובץ מפתח (GnuPG)"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:48001
msgid "Random key"
msgstr "מפתח אקראי"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "Unsafe swap space detected"
msgstr "זוהה שטח החלפה לא מאובטח"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "An unsafe swap space has been detected."
msgstr "זוהה שטח החלפה לא מאובטח."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"This is a fatal error since sensitive data could be written out to disk "
"unencrypted. This would allow someone with access to the disk to recover "
"parts of the encryption key or passphrase."
msgstr ""
"מדובר בשגיאה חמורה כיוון שעלולים להיכתב לכונן נתונים רגישים ללא הצפנה. מצב "
"זה יאפשר לכל מי שיש לו גישה לכונן לשחזר חלקים ממפתח ההצפנה או ממילת הצופן."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"Please disable the swap space (e.g. by running swapoff) or configure an "
"encrypted swap space and then run setup of encrypted volumes again. This "
"program will now abort."
msgstr ""
"נא להשבית את שטח ההחלפה (לדוגמה: על ידי הרצה של swapoff) או להגדיר שטח החלפה "
"מוצפן. לאחר מכן יש להריץ את התקנת הכרכים המוצפנים מחדש. תכנית זאת תסיים "
"עכשיו."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "Encryption passphrase:"
msgstr "מילת הצופן של ההצפנה:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "You need to choose a passphrase to encrypt ${DEVICE}."
msgstr "יש לבחור מילת צופן כדי להצפין את ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid ""
"The overall strength of the encryption depends strongly on this passphrase, "
"so you should take care to choose a passphrase that is not easy to guess. It "
"should not be a word or sentence found in dictionaries, or a phrase that "
"could be easily associated with you."
msgstr ""
"חוזק ההצפנה הכולל תלוי ברובו על מילת צופן זו, לכן מוטב שלא להקל ראש בבחירת "
"מילת הצופן כדי להקשות על ניחושה. עדיף שלא להשתמש במילה או צירוף מילים שניתן "
"למצוא במילון או ביטוי שניתן לקשר אליך בקלות."

#. Type: password
#. Description
#. :sl3:
#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001 ../partman-crypto.templates:55001
msgid ""
"A good passphrase will contain a mixture of letters, numbers and "
"punctuation. Passphrases are recommended to have a length of 20 or more "
"characters."
msgstr ""
"מילת צופן מוצלחת תכיל שילוב של אותיות, מספרים וניקוד. האורך המומלץ של מילות "
"צופן הוא 20 תווים ומעלה."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid "Re-enter passphrase to verify:"
msgstr "נא להקליד את מילת הצופן מחדש לאימות:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid ""
"Please enter the same passphrase again to verify that you have typed it "
"correctly."
msgstr "נא להקליד את אותה מילת הצופן כדי לאמת שהקלדת אותה כראוי."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "Passphrase input error"
msgstr "שגיאת קלט עם מילת הצופן"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "The two passphrases you entered were not the same. Please try again."
msgstr "שתי מילות הצופן שהקלדת שונות זו מזו. נא לנסות שוב."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid "Empty passphrase"
msgstr "מילת צופן ריקה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid ""
"You entered an empty passphrase, which is not allowed. Please choose a non-"
"empty passphrase."
msgstr ""
"מילת הצופן שהוקלדה ריקה, אסור להשאיר אותה ריקה. נא לבחור במילת צופן שאינה "
"ריקה."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:54001
msgid "Use weak passphrase?"
msgstr "להשתמש במילת צופן חלשה?"

#. Type: boolean
#. Description
#. :sl3:
#. Translators: we unfortunately cannot use plural forms here
#. So, you are suggested to use the plural form adapted for
#. MINIMUM=8, which is the current hardcoded value
#: ../partman-crypto.templates:54001
msgid ""
"You entered a passphrase that consists of less than ${MINIMUM} characters, "
"which is considered too weak. You should choose a stronger passphrase."
msgstr ""
"מילת הצופן שהקלדת מורכבת מפחות מ־${MINIMUM} תווים, הרכב שנחשב חלש. עליך "
"לבחור במילת צופן חזקה יותר."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid "OPAL admin password:"
msgstr "סיסמת ניהול OPAL:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"You need to choose a new, or provide the existing, OPAL admin password for "
"${DEVICE}."
msgstr "יש לבחור סיסמת ניהול OPAL קיימת או חדשה עבור ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"This password will be used to manage the whole device - add new OPAL "
"partitions, remove them, erase them. If this password is lost, the device "
"will need to be factory reset to be recovered, losing all data from all "
"partitions."
msgstr ""
"הסיסמה הזאת תשמש לנהל את כל ההתקן - להוסיף מחיצות OPAL חדשות, להסיר אותן, "
"למחוק אותן. אם הסיסמה הזאת תלך לאיבוד, ההתקן יצטרך איפוס יצרן כדי להשתקם תוך "
"איבוד כל הנתונים מכל המחיצות."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
msgid "Re-enter OPAL admin password to verify:"
msgstr "נא למלא את סיסמת ניהול ה־OPAL שוב לצורך אימות:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
msgid ""
"Please enter the same password again to verify that you have typed it "
"correctly."
msgstr "נא להקליד את אותה הסיסמה שוב כדי לוודא שהוקלדה נכון."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "Password input error"
msgstr "שגיאה בקליטת הסיסמה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "שתי הסיסמאות שמילאת אינן זהות. נא לנסות שוב."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid "Empty password"
msgstr "סיסמה ריקה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr "מילאת סיסמה ריקה, אסור לעשות זאת. נא לבחור סיסמה לא ריקה."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid "OPAL PSID:"
msgstr "PSID (מזהה אבטחה פיזי) של OPAL:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid ""
"You need to provide the OPAL PSID for ${DEVICE}. This is typically found on "
"the drive's label."
msgstr ""
"יש לספק את ה־PSID (מזהה אבטחה פיזי) של ה־OPAL עבור ${DEVICE}. זה בדרך כלל "
"נמצא על תווית הכונן."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
msgid "Re-enter OPAL PSID to verify:"
msgstr "נא למלא את ה־PSID (מזהה אבטחה פיזי) של OPAL לצורך אימות:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
msgid ""
"Please enter the same PSID again to verify that you have typed it correctly."
msgstr "נא להקליד את אותו PSID (מזהה אבטחה פיזי) כדי לאמת שהקלדת אותו כראוי."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
msgid "OPAL PSID input error"
msgstr "שגיאה בקליטת OPAL PSID"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
msgid "The two OPAL PSIDs you entered were not the same. Please try again."
msgstr "שני ה־OPAL PSID שמילאת אינם זהים. נא לנסות שוב."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
msgid "Empty OPAL PSID"
msgstr "OPAL PSID ריק"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
msgid ""
"You entered an empty PSID, which is not allowed. Please provide a non-empty "
"PSID."
msgstr "מילאת PSID ריק, אסור לעשות זאת. נא לספק PSID לא ריק."

#. Type: entropy
#. Description
#. :sl3:
#: ../partman-crypto.templates:63001
msgid "The encryption key for ${DEVICE} is now being created."
msgstr "כרגע נוצר מפתח הצפנה עבור ${DEVICE}."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:64001
msgid "Key data has been created successfully."
msgstr "מידע על המפתח נוצר בהצלחה."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "Keyfile creation failure"
msgstr "יצירת קובץ המפתח נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "An error occurred while creating the keyfile."
msgstr "אירעה שגיאה בעת יצירת קובץ המפתח."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
msgid "Configure encryption without separate /boot?"
msgstr "להגדיר הצפנה בלי מחיצת ‎/boot נפרדת?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
msgid ""
"You have selected the root file system to be stored on an encrypted "
"partition. When using GRUB, usually this feature requires a separate /boot "
"partition on which the kernel and initrd can be stored. This is not required "
"when using systemd-boot/UKIs, as the ESP is used instead."
msgstr ""
"בחרת למקם את שורש מערכת הקבצים במחיצה מוצפנת. בעת שימוש ב־GRUB התכונה זו "
"דורשת מחיצת ‎/boot נפרדת בה יאוחסנו הליבה וה־initrd. לא נחוץ עם systemd-boot/"
"UKIs עקב שימוש ב־ESP במקום."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid "Encryption configuration failure"
msgstr "שגיאה בהגדרת הצפנה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You have selected the /boot file system to be stored on an encrypted "
"partition. This is not possible because the boot loader would be unable to "
"load the kernel and initrd. Continuing now would result in an installation "
"that cannot be used."
msgstr ""
"בחרת לאחסן את מערכת הקבצים ‎/boot על מחיצה מוצפנת. מצב זה אינו אפשרי כיוון "
"שמערכת האתחול אמורה לטעון את הליבה ואת ה־initrd. המשך בתהליך עשוי להוביל "
"למערכת בלתי שמישה."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You should go back and choose a non-encrypted partition for the /boot file "
"system."
msgstr "עליך לחזור אחורה ולבחור במחיצה בלתי מוצפנת עבור מערכת הקבצים ‎/boot."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid "Are you sure you want to use a random key?"
msgstr "להשתמש במפתח אקראי?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"You have chosen a random key type for ${DEVICE} but requested the "
"partitioner to create a file system on it."
msgstr ""
"בחרת בסוג מפתח אקראי עבור התקן ${DEVICE}, אבל ביקשת ממנהל המחיצות ליצור עליו "
"מערכת קבצים."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"Using a random key type means that the partition data is going to be "
"destroyed upon each reboot. This should only be used for swap partitions."
msgstr ""
"משמעות השימוש בסוג מפתח אקראי היא שנתוני המחיצה יושמדו בכל הפעלה מחדש. כדאי "
"להשתמש בתכונה זו למחיצות החלפה בלבד."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "Failed to download crypto components"
msgstr "הורדת רכיבי ההצפנה נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "An error occurred trying to download additional crypto components."
msgstr "אירעה שגיאה במהלך הניסיון להורדת רכיבי הצפנה נוספים."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid "Proceed to install crypto components despite insufficient memory?"
msgstr "להמשיך בהתקנת רכיבי ההצפנה למרות שאין מספיק זיכרון?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid ""
"There does not seem to be sufficient memory available to install additional "
"crypto components. If you choose to go ahead and continue anyway, the "
"installation process could fail."
msgstr ""
"נראה שאין מספיק מקום פנוי בזיכרון כדי להתקין רכיבי הצפנה נוספים. בחירה "
"להמשיך בתהליך בכל זאת עשויה להוביל להכשלת ההתקנה."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Create encrypted volumes"
msgstr "יצירת כרכים מוצפנים"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Finish"
msgstr "סיום"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "Encryption configuration actions"
msgstr "פעולות להגדרת הצפנה"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "This menu allows you to configure encrypted volumes."
msgstr "תפריט זה מאפשר לך להגדיר כרכים מוצפנים."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Devices to encrypt:"
msgstr "התקנים להצפנה:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Please select the devices to be encrypted."
msgstr "נא לבחור התקנים להצפנה."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "You can select one or more devices."
msgstr "ניתן לבחור התקן אחד או יותר."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices selected"
msgstr "לא נבחרו התקנים"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices were selected for encryption."
msgstr "לא נבחרו התקנים להצפנה."
