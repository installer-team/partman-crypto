# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of eu.po to Euskara
# Basque messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Translations from iso-codes:
# Copyright (C)
# Translations from KDE:
# Piarres Beobide <pi@beobide.net>, 2004-2009, 2011.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2008, 2010.
# Mikel Olasagasti <hey_neken@mundurat.net>, 2004.
# Piarres Beobide Egaña <pi@beobide.net>, 2004,2006,2007, 2008, 2009.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2010.
# Free Software Foundation, Inc., 2002.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
# Marcos Goienetxe <marcos_g@infonegocio.com>, 2002.
# Piarres Beobide <pi@beobide.net>, 2008.
# Xabier Bilbao <xabidu@gmail.com>, 2008.
# Iñaki Larrañaga Murgoitio <dooteo@zundan.com>, 2020.
# Gontzal Manuel Pujana Onaindia <thadahdenyse@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: di-sublevel1_eu\n"
"Report-Msgid-Bugs-To: partman-crypto@packages.debian.org\n"
"POT-Creation-Date: 2024-05-07 20:02+0000\n"
"PO-Revision-Date: 2022-06-09 19:45+0000\n"
"Last-Translator: Gontzal Manuel Pujana Onaindia <thadahdenyse@gmail.com>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. File system name
#. Keep translations short enough
#. :sl3:
#: ../partman-crypto.templates:1001
msgid "physical volume for encryption"
msgstr "bolumen fisikoa enkriptatzeko"

#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#. Should be kept very short or unstranslated
#. :sl3:
#: ../partman-crypto.templates:2001
msgid "crypto"
msgstr "enkript"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:3001
msgid "Device-mapper (dm-crypt)"
msgstr "Gailu-mapatzailea (dm-crypt)"

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. Translations should be kept below 40 columns
#. :sl3:
#: ../partman-crypto.templates:4001
msgid ""
"Firmware-backed self-encrypting disk (vendor-implemented OPAL with LUKS2)"
msgstr ""

#. Type: text
#. Description
#. This is related to "encryption method"
#. Encryption type for a file system
#. :sl3:
#: ../partman-crypto.templates:6001
msgid "not active"
msgstr "ez aktiboa"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:7001
msgid "Encryption method:"
msgstr "Enkriptatze metodoa:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid "Encryption method for this partition:"
msgstr "Partizio honen enkriptatze metodoa:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:8001
msgid ""
"Changing the encryption method will set other encryption-related fields to "
"their default values for the new encryption method."
msgstr ""
"Enkriptatze metodoa aldatzean enkriptatze metodo berriarekin zerikusia duten "
"beste eremuak beraien balio lehenetsiekin konfiguratuko dira."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:9001
msgid "Encryption:"
msgstr "Enkriptaketa:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:10001
msgid "Encryption for this partition:"
msgstr "Partizio honen enkriptaketa:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:11001
msgid "Key size:"
msgstr "Gako tamaina::"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:12001
msgid "Key size for this partition:"
msgstr "Partizio honen gako tamaina:"

#. Type: text
#. Description
#. An initialization vector is the initial value used to seed
#. the encryption algorithm
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:13001
msgid "IV algorithm:"
msgstr "IV algoritmoa:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid "Initialization vector generation algorithm for this partition:"
msgstr "Partizio honen hasierako bektorea sortzeko algoritmoa:"

#. Type: select
#. Description
#. An initialization vector is the initial randomness used to seed
#. the encryption algorithm
#. :sl3:
#: ../partman-crypto.templates:14001
msgid ""
"Different algorithms exist to derive the initialization vector for each "
"sector. This choice influences the encryption security. Normally, there is "
"no reason to change this from the recommended default, except for "
"compatibility with older systems."
msgstr ""
"Sektore bakoitzaren hasierako bektorearen arabera algoritmo desberdinak "
"daude. Aukera honek enkriptaketaren segurtasunean eragiten du. Normalean ez "
"dago gomendatutako hobespenak aldatzeko arrazoirik, sistema zaharragoekin "
"bateragarritasuna mantentzeko ezik."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:15001
msgid "Encryption key:"
msgstr "Enkriptatze gakoa:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:16001
msgid "Type of encryption key for this partition:"
msgstr "Partizio honen enkriptatze gako mota:"

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#: ../partman-crypto.templates:17001
msgid "Encryption key hash:"
msgstr "Enkript.-gakoaren hash:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid "Type of encryption key hash for this partition:"
msgstr "Partizio honen enkriptatze gakoaren hash mota:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:18001
msgid ""
"The encryption key is derived from the passphrase by applying a one-way hash "
"function to it. Normally, there is no reason to change this from the "
"recommended default and doing so in the wrong way can reduce the encryption "
"strength."
msgstr ""
"Enkriptaketaren gakoa pasaesalditik eratorrita dago, norabide bakarreko hash "
"funtzio bat aplikatu ondoren. Normalean ez dago lehenespen hau aldatzeko "
"arrazoirik, eta aldaketa hori modu okerrean egiteak enkriptaketaren indarra "
"gutxitu dezake."

#. Type: text
#. Description
#. Should be kept below 24 columns
#. :sl3:
#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:19001 ../partman-crypto.templates:20001
msgid "OPAL self-encryption nested with dm-crypt:"
msgstr ""

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:20001
msgid ""
"Some disks support self-encryption. This is a hardware feature that, if "
"enabled via this option, will be used together with dm-crypt to provide a "
"double layer of encryption."
msgstr ""

#. Type: text
#. Description
#. This shows up in a screen summarizing options and will be followed
#. by "yes" or "no"
#. :sl3:
#: ../partman-crypto.templates:21001
msgid "Erase data:"
msgstr "Ezabatu datuak:"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:22001
msgid "no"
msgstr "ez"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:23001
msgid "yes"
msgstr "bai"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:24001
msgid "Erase data on this partition"
msgstr "Ezabatu partizio honetako datuak"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:25001
msgid "Factory reset this entire disk (ALL data will be lost)"
msgstr ""

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001 ../partman-crypto.templates:30001
msgid "Really erase the data on ${DEVICE}?"
msgstr "Ziur zaude ${DEVICE}(e)ko datuak ezabatu nahi dituzula?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:26001
msgid ""
"The data on ${DEVICE} will be overwritten with zeroes. It can no longer be "
"recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"${DEVICE} gailuko datuak ausazko datuekin gainidatziko dira. Behin urrats "
"hau amaitu eta gero ezin izango dira berreskuratu. Hau azken aukera da "
"ezabaketa abortatzeko."

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:27001 ../partman-crypto.templates:31001
msgid "Erasing data on ${DEVICE}"
msgstr "${DEVICE}(e)ko datuak ezabatzen"

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:28001
msgid ""
"The installer is now overwriting ${DEVICE} with zeroes to delete its "
"previous contents. This step may be skipped by cancelling this action."
msgstr ""
"Instalatzaileak ${DEVICE} gailua zeroekin gainidatziko du aurreko edukia "
"ezabatzeko. Urrats hau salta dezakezu uneko ekintza bertan behera utziz."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001 ../partman-crypto.templates:33001
msgid "Erasing data on ${DEVICE} failed"
msgstr "Huts egin du ${DEVICE}(e)ko datuak ezabatzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:29001
msgid ""
"An error occurred while trying to overwrite the data on ${DEVICE} with "
"zeroes. The data has not been erased."
msgstr ""
"Errorea gertatu da ${DEVICE}(e)ko datuak zeroekin gainidaztean. Datuak ez "
"dira ezabatu."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:30001
msgid ""
"The data on ${DEVICE} will be overwritten with random data. It can no longer "
"be recovered after this step has completed. This is the last opportunity to "
"abort the erase."
msgstr ""
"${DEVICE} gailuko datuak ausazko datuekin gainidatzi egingo dira. Behin "
"urrats hau amaitu eta gero ezin izango dira berreskuratu. Hau azken aukera "
"da ezabaketa abortatzeko."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:32001
msgid ""
"The installer is now overwriting ${DEVICE} with random data to prevent meta-"
"information leaks from the encrypted volume. This step may be skipped by "
"cancelling this action, albeit at the expense of a slight reduction of the "
"quality of the encryption."
msgstr ""
"Instalatzaileak ${DEVICE} gailua ausazko datuekin gainidatziko du "
"enkriptatutako bolumenaren meta-informazioaren ihesia saihesteko. Urrats hau "
"salta dezakezu uneko ekintza bertan behera utziz, enkriptaketaren kalitatea "
"pixka bat jaistearen kaltetan izan arren."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:33001
msgid ""
"An error occurred while trying to overwrite ${DEVICE} with random data. "
"Recovery of the device's previous contents is possible and meta-information "
"of its new contents may be leaked."
msgstr ""
"Errore bat gertatu da ${DEVICE} gailua ausazko datuekin gainidaztean. "
"Gailuaren aurreko edukia berreskura daiteke, eta bere eduki berriaren meta-"
"informazioa ihes daiteke."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:34001
msgid "Setting up encryption..."
msgstr "Enkriptaketa konfiguratzea..."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:35001
msgid "Configure encrypted volumes"
msgstr "Konfiguratu bolumen enkriptatuak"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions to encrypt"
msgstr "Ez dago partiziorik enkriptatzeko"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:36001
msgid "No partitions have been selected for encryption."
msgstr "Ez da partiziorik hautatu enkriptatzeko."

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid "Required programs missing"
msgstr "Beharrezko programak falta dira"

#. Type: note
#. Description
#. :sl3:
#: ../partman-crypto.templates:37001
msgid ""
"This build of debian-installer does not include one or more programs that "
"are required for partman-crypto to function correctly."
msgstr ""
"Debian instalatzailearen bertsio honi programa bat (edo gehiago) falta zaio "
"partman-crypt behar bezala funtzionatzeko."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid "Required encryption options missing"
msgstr "Enkriptaketaren beharrezko aukerak falta dira"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:38001
msgid ""
"The encryption options for ${DEVICE} are incomplete. Please return to the "
"partition menu and select all required options."
msgstr ""
"${DEVICE}(r)en enkriptaketaren aukerak ez osatu gabe daude. Itzuli "
"partizioen menura eta hautatu beharrezkoak diren aukera guztiak."

#. Type: text
#. Description
#. :sl3:
#. Translators: this string is used to assemble a string of the format
#. "$specify_option: $missing". If this proves to be a problem in your
#. language, please contact the maintainer and we can do it differently.
#: ../partman-crypto.templates:39001
msgid "missing"
msgstr "falta da"

#. Type: text
#. Description
#. :sl3:
#. What is "in use" is a partition
#: ../partman-crypto.templates:40001
msgid "In use as physical volume for encrypted volume ${DEV}"
msgstr "${DEV} enkriptatutako bolumenaren bolumen fisiko gisa erabilia"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid "Encryption package installation failure"
msgstr "Huts egin du enkriptatze-paketea instalatzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"The kernel module package ${PACKAGE} could not be found or an error occurred "
"during its installation."
msgstr ""
"Ezin izan da nukleo-moduluaren ${PACKAGE} paketea aurkitu edo errorea "
"gertatu da instalatzean."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:41001
msgid ""
"It is likely that there will be problems setting up encrypted partitions "
"when the system is rebooted. You may be able to correct this by installing "
"the required package(s) later on."
msgstr ""
"Sistema berrabiarazi eta gero, enkriptatutako partizioak konfiguratzean "
"arazoak gerta daitezke. Arazo horiek konpontzeko beharrezko paketea(k) "
"geroago instala ditzakezu."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid "Write the changes to disk and configure encrypted volumes?"
msgstr "Aldaketak diskoan idatzi eta bolumen enkriptatuak konfiguratu?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001
msgid ""
"Before encrypted volumes can be configured, the current partitioning scheme "
"has to be written to disk.  These changes cannot be undone."
msgstr ""
"Uneko partizio-eskema diskoan idatzi behar da bolumen enkriptatuak "
"konfiguratu aurretik. Aldaketa horiek ezin dira desegin."

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:42001 ../partman-crypto.templates:43001
msgid ""
"After the encrypted volumes have been configured, no additional changes to "
"the partitions on the disks containing encrypted volumes are allowed. Please "
"decide if you are satisfied with the current partitioning scheme for these "
"disks before continuing."
msgstr ""
"Bolumen enkriptatuak konfiguratu ondoren, ezin dira partizioetan aldaketa "
"gehiagorik egin diskoen (bolumen enkriptatuak dituztenak) partizioetan. "
"Jarraitu aurretik erabaki gustura zauden disko hauen uneko partizio-"
"eskemarekin."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:43001
msgid "Keep current partition layout and configure encrypted volumes?"
msgstr "Uneko partizio-diseinua mantendu eta bolumen enkriptatuak konfiguratu?"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "Configuration of encrypted volumes failed"
msgstr "Huts egin du bolumen enkriptatuak konfiguratzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "An error occurred while configuring encrypted volumes."
msgstr "Errorea gertatu da bolumen enkriptatuak konfiguratzean."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:44001
msgid "The configuration has been aborted."
msgstr "Konfigurazioa abortatu egin da."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "Initialisation of encrypted volume failed"
msgstr "Huts egin du enkriptatutako bolumena hasieratzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:45001
msgid "An error occurred while setting up encrypted volumes."
msgstr "Errorea gertatu da bolumen enkriptatuak konfiguratzean."

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:46001
msgid "Passphrase"
msgstr "Pasaesaldia"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:47001
msgid "Keyfile (GnuPG)"
msgstr "Gako-fitxategia (GnuPG)"

#. Type: text
#. Description
#. :sl3:
#. This is a key type for encrypted file systems
#. It can be either protected by a passphrase, a keyfile
#. of a random key
#. This text is one of these choices, so keep it short
#: ../partman-crypto.templates:48001
msgid "Random key"
msgstr "Ausazko gakoa"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "Unsafe swap space detected"
msgstr "Segurtasun gabeko swap lekua aurkitu da"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid "An unsafe swap space has been detected."
msgstr "Segurua ez den swap lekua detektatu da."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"This is a fatal error since sensitive data could be written out to disk "
"unencrypted. This would allow someone with access to the disk to recover "
"parts of the encryption key or passphrase."
msgstr ""
"Hau errore larria da, informazio pribatua enkriptatu gabeko diskotik kanpora "
"idatz daitekeelako. Diskora sarbidetza duen norbaiti enkriptaketaren "
"pasaesaldia edo gakoa eskuratzeko aukera emango baitio."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:49001
msgid ""
"Please disable the swap space (e.g. by running swapoff) or configure an "
"encrypted swap space and then run setup of encrypted volumes again. This "
"program will now abort."
msgstr ""
"Desgaitu swap lekua (adib., swapoff exekutatuz) edo konfiguratu "
"enkriptatutako swap leku bat, eta ondoren exekutatu enkriptatutako bolumenen "
"konfigurazioa berriro. Programa hau orain abortatu egingo da."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "Encryption passphrase:"
msgstr "Enkriptaketaren pasaesaldia:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid "You need to choose a passphrase to encrypt ${DEVICE}."
msgstr "${DEVICE} enkriptatzeko pasaesaldia aukeratu behar duzu."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001
msgid ""
"The overall strength of the encryption depends strongly on this passphrase, "
"so you should take care to choose a passphrase that is not easy to guess. It "
"should not be a word or sentence found in dictionaries, or a phrase that "
"could be easily associated with you."
msgstr ""
"Enkriptaketaren indar orokorra pasaesaldi honen mende dago bete betean, eta "
"horregatik asmatzeko erraza ez den pasaesaldia aukeratzea komeni da. Ez du "
"hiztegiko hitza izan behar, ezta zurekin zerikusia duena ere (adibidez, "
"bigarren deitura)."

#. Type: password
#. Description
#. :sl3:
#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:50001 ../partman-crypto.templates:55001
msgid ""
"A good passphrase will contain a mixture of letters, numbers and "
"punctuation. Passphrases are recommended to have a length of 20 or more "
"characters."
msgstr ""
"Pasaesaldi on batek letra, zenbaki eta puntuazio ikurren nahasketa bat eduki "
"beharko luke. Pasaesaldien luzerak 20 karaktere edo gehiago izatea "
"gomendatzen da."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid "Re-enter passphrase to verify:"
msgstr "Sartu berriro pasaesaldia egiaztatzeko:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:51001
msgid ""
"Please enter the same passphrase again to verify that you have typed it "
"correctly."
msgstr "Sartu pasaesaldi berdina berriro, ondo idatzi duzula egiaztatzeko."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "Passphrase input error"
msgstr "Errorea pasaesaldiaren sarreran"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:52001
msgid "The two passphrases you entered were not the same. Please try again."
msgstr "Sartutako bi pasaesaldiak ez dira berdinak. Saiatu berriro."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid "Empty passphrase"
msgstr "Pasaesaldi hutsa"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:53001
msgid ""
"You entered an empty passphrase, which is not allowed. Please choose a non-"
"empty passphrase."
msgstr ""
"Onartzen ez den pasaesaldi hutsa sartu duzu. Aukeratu hutsa ez den "
"pasaesaldia."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:54001
msgid "Use weak passphrase?"
msgstr "Pasaesaldi ahula erabili?"

#. Type: boolean
#. Description
#. :sl3:
#. Translators: we unfortunately cannot use plural forms here
#. So, you are suggested to use the plural form adapted for
#. MINIMUM=8, which is the current hardcoded value
#: ../partman-crypto.templates:54001
msgid ""
"You entered a passphrase that consists of less than ${MINIMUM} characters, "
"which is considered too weak. You should choose a stronger passphrase."
msgstr ""
"Gomendatutako ${MINIMUM} karaktere baino gutxiago dituen pasaesaldia "
"aukeratu duzu, eta ahulegitzat jotzen da. Pasaesaldi luzeagoa erabiltzea "
"pentsatu beharko zenuke."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid "OPAL admin password:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"You need to choose a new, or provide the existing, OPAL admin password for "
"${DEVICE}."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:55001
msgid ""
"This password will be used to manage the whole device - add new OPAL "
"partitions, remove them, erase them. If this password is lost, the device "
"will need to be factory reset to be recovered, losing all data from all "
"partitions."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
#, fuzzy
msgid "Re-enter OPAL admin password to verify:"
msgstr "Berretsi pasahitza:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:56001
#, fuzzy
msgid ""
"Please enter the same password again to verify that you have typed it "
"correctly."
msgstr ""
"Sartu root-aren pasahitz berdina berriro, ongi idatzi duzula ziurtatzeko."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "Password input error"
msgstr "Pasahitzaren sarreraren errorea"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:57001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Sartu dituzun bi pasahitzak ez dira berdinak. Saiatu berriro."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid "Empty password"
msgstr "Pasahitz hutsa"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:58001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Pasahitz hutsa sartu duzu, eta ez dago onartuta. Hautatu hutsik ez dagoen "
"pasahitza."

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid "OPAL PSID:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:59001
msgid ""
"You need to provide the OPAL PSID for ${DEVICE}. This is typically found on "
"the drive's label."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
#, fuzzy
msgid "Re-enter OPAL PSID to verify:"
msgstr "Berretsi pasahitza:"

#. Type: password
#. Description
#. :sl3:
#: ../partman-crypto.templates:60001
#, fuzzy
msgid ""
"Please enter the same PSID again to verify that you have typed it correctly."
msgstr "Sartu pasaesaldi berdina berriro, ondo idatzi duzula egiaztatzeko."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
#, fuzzy
msgid "OPAL PSID input error"
msgstr "Pasahitzaren sarreraren errorea"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:61001
#, fuzzy
msgid "The two OPAL PSIDs you entered were not the same. Please try again."
msgstr "Sartu dituzun bi pasahitzak ez dira berdinak. Saiatu berriro."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
msgid "Empty OPAL PSID"
msgstr ""

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:62001
#, fuzzy
msgid ""
"You entered an empty PSID, which is not allowed. Please provide a non-empty "
"PSID."
msgstr ""
"Pasahitz hutsa sartu duzu, eta ez dago onartuta. Hautatu hutsik ez dagoen "
"pasahitza."

#. Type: entropy
#. Description
#. :sl3:
#: ../partman-crypto.templates:63001
msgid "The encryption key for ${DEVICE} is now being created."
msgstr "${DEVICE} gailuaren enkriptaketaren gakoa orain sortuko da."

#. Type: text
#. Description
#. :sl3:
#: ../partman-crypto.templates:64001
msgid "Key data has been created successfully."
msgstr "Gakoaren datuak behar bezala sortu dira."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "Keyfile creation failure"
msgstr "Huts egin du gako-fitxategia sortzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:65001
msgid "An error occurred while creating the keyfile."
msgstr "Errorea gertatu da gako-fitxategia sortzean."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
#, fuzzy
msgid "Configure encryption without separate /boot?"
msgstr "btrfs fitxategi-sistema ez da onartzen banandutako /boot bat gabe"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:66001
#, fuzzy
msgid ""
"You have selected the root file system to be stored on an encrypted "
"partition. When using GRUB, usually this feature requires a separate /boot "
"partition on which the kernel and initrd can be stored. This is not required "
"when using systemd-boot/UKIs, as the ESP is used instead."
msgstr ""
"Erroko fitxategi-sistema enkriptatutako partizio batean gordetzea hautatu "
"duzu. Eginbide honek bereiztutako /boot partizio bat behar du nukleoa eta "
"initrd irudia biltegiratzeko."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid "Encryption configuration failure"
msgstr "Huts egin du enkriptaketa konfiguratzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You have selected the /boot file system to be stored on an encrypted "
"partition. This is not possible because the boot loader would be unable to "
"load the kernel and initrd. Continuing now would result in an installation "
"that cannot be used."
msgstr ""
"/boot fitxategi-sistema enkriptatutako partizio batean gordetzea hautatu "
"duzu. Hau ezinezkoa da, abioko kargatzaileak ezin izango baitu nukleoa eta "
"initrd kargatu. Aurrera jarraitzen baduzu, ezin izango da instalazioa "
"erabili."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:67001
msgid ""
"You should go back and choose a non-encrypted partition for the /boot file "
"system."
msgstr ""
"Joan atzera eta aukeratu enkriptatu gabeko partizio bat /boot fitxategi-"
"sistema ezartzeko."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid "Are you sure you want to use a random key?"
msgstr "Ziur zaude ausazko gakoa erabiltzea nahi duzula?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"You have chosen a random key type for ${DEVICE} but requested the "
"partitioner to create a file system on it."
msgstr ""
"Ausazko gakoa erabiltzea aukeratu duzu ${DEVICE}(r)entzat, baina "
"partiziogileari bertan fitxategi-sistema bat sortzeko eskatu diozu."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:68001
msgid ""
"Using a random key type means that the partition data is going to be "
"destroyed upon each reboot. This should only be used for swap partitions."
msgstr ""
"Ausazko gako bat erabiltzeak berrabiarazte bakoitzean partizioaren datuak "
"ezabatzea eragingo du. Hau swap partizioetan bakarrik erabili beharko "
"litzateke."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "Failed to download crypto components"
msgstr "Huts egin du enkriptaketaren osagaia deskargatzean"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:69001
msgid "An error occurred trying to download additional crypto components."
msgstr "Errorea gertatu da enkriptaketaren osagai gehigarriak deskargatzean."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid "Proceed to install crypto components despite insufficient memory?"
msgstr ""
"Nahiz eta nahikoa memoria ez egon, enkriptaketaren osagaien instalazioarekin "
"jarraitu?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-crypto.templates:70001
msgid ""
"There does not seem to be sufficient memory available to install additional "
"crypto components. If you choose to go ahead and continue anyway, the "
"installation process could fail."
msgstr ""
"Badirudi ez dagoela nahikoa memoria enkriptaketaren osagai gehigarriak "
"instalatzeko. Aurrera jarraitzea hautatzen baduzu, instalazioko prozesuak "
"huts egin dezake."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Create encrypted volumes"
msgstr "Sortu bolumen enkriptatuak"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-crypto.templates:71001
msgid "Finish"
msgstr "Amaitu"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "Encryption configuration actions"
msgstr "Enkriptaketaren konfigurazioko ekintzak"

#. Type: select
#. Description
#. :sl3:
#: ../partman-crypto.templates:71002
msgid "This menu allows you to configure encrypted volumes."
msgstr "Menu honek bolumen enkriptatuak konfiguratzeko aukera ematen dizu."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Devices to encrypt:"
msgstr "Enkriptatzeko gailuak:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "Please select the devices to be encrypted."
msgstr "Hautatu enkriptatuko diren gailuak."

#. Type: multiselect
#. Description
#. :sl3:
#: ../partman-crypto.templates:72001
msgid "You can select one or more devices."
msgstr "Gailu bat edo gehiago hauta ditzakezu."

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices selected"
msgstr "Ez da gailurik hautatu"

#. Type: error
#. Description
#. :sl3:
#: ../partman-crypto.templates:73001
msgid "No devices were selected for encryption."
msgstr "Ez da gailurik hautatu enkriptatzeko."
